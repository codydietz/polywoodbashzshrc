# Polywood Bash/ZSH RC file

RC file for bash or ZSH with a lot of useful commands.

## Installing

Simply copy the .bashrc file into your linux home directory as either .bashrc (for bash) or .zshrc (for zsh).