#################################################
# Change this to your default virtualenv folder #
#################################################
export WORKON_HOME='~/Envs'

#############################
# Do platform specific things #
#############################
if [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    # Do something under GNU/Linux platform
    plugins=(git bundler pip python git-flow jsontools pyenv sudo)
elif [ "$(expr substr $(uname -s) 1 10)" = "MINGW32_NT" ]; then
    # Do something under Windows NT platform if you are a terrible person
fi

###########################################
# Alias some of my commonly used commands #
###########################################
alias grep='grep --color=auto'
alias untar='tar -zxvf'
alias cddev='cd ~/Development'
alias sshdev='ssh polywooddev@vm-lnx-devlamp1'
alias sshprod='ssh polywood@vm-lnx-web1'
alias gitdiff='git diff | grep -i "+++"'
alias resource='source ~/.zshrc'
alias vi='vim'
alias lsd='ls -d */'
alias vimrc='vim ~/.zshrc'
alias findtext='grep -rsn . -e'
alias cd-='cd -'
alias editcron='env EDITOR=vim crontab -e'
alias curltor='curl --socks5-hostname localhost:9150'
alias composer="php /usr/local/bin/composer.phar"
alias rsync='rsync --progress'
alias sshgen='ssh-keygen -t rsa -C'
alias speedtest='curl -s  https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python'
alias mkvirtualenv3='mkvirtualenv --python=`which python3`'
alias gitdeletebranch='git push --delete'

##########################
# Personal dev functions #
##########################
function lsn(){
    ls -U | head -$1
}

function lstn(){
    ls -t -U | head - $1
}

function gitdiffall(){
    for D in `find . ! -path . -type d -maxdepth 1`
    do
    	echo "Checking: $D"
    	cd $D
    	gitdiff
    	cd ..
    done
}

function gitrenamebranch() {
    
    git branch -m $1 $2                 # Rename branch locally    
    git push origin :$1                 # Delete the old branch    
    git push --set-upstream origin $2   # Push the new branch, set local branch to track the new remote
    
}

function rmndaysold(){
    if [ "$#" -eq 1 ]; then
        find . -mtime +$1 -exec rm {} \;
    elif [ "$#" -eq 2 ]; then
        find $1 -mtime +$2 -exec rm {} \;
    fi
}

function addsshkey(){
    if [ "$#" -eq 2 ]; then
        cat ~/.ssh/id_rsa.pub | ssh $1@$2 "mkdir -p ~/.ssh && cat >>  ~/.ssh/authorized_keys" 
    elif [ "$#" -eq 3 ]; then
        cat ~/.ssh/id_rsa.pub | ssh -p $3 $1@$2 "mkdir -p ~/.ssh && cat >>  ~/.ssh/authorized_keys"
    fi
}

function dircount(){
    if [ "$#" -ne 1 ]; then
        ls -l | wc -l
    else
        ls -l $1 | wc -l
    fi
}

function renameall(){
    for i in *; do echo mv $i ${i/$1/$2}; done | sh
}

function topid(){
    top -pid $(pgrep $1 | head -20 | tr "\\n" "," | sed 's/,$//')
}

function gitpushall(){
	git add .
	git commit -m $1
	git push
}

function gitpush(){
	git commit -m $1
	git push
}

# Make directory and immediately cd into it
function mkd(){
    mkdir -p "$@" && cd "$@"
}

# Checks for zombie processes
function zombie(){
    ps aux | awk '{if ($8=='Z') {print $2}}'
}

# Function to check for what npm packages are installed
function npmls(){
    npm ls --depth=0 "$@" 2>/dev/null
}

##########################################################
# You may need to manually set your language environment #
##########################################################
export LANG=en_US.UTF-8

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"
